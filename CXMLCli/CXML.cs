﻿using System;

namespace CXML
{
    public enum AttributeTypePs3
    {
        TYPE_NONE = 0,
        TYPE_INT = 1,
        TYPE_FLOAT = 2,
        TYPE_STRING = 3,
        TYPE_INTEGER_ARRAY = 4,
        TYPE_FLOAT_ARRAY = 5,
        TYPE_FILE = 6,
        TYPE_ID_REF = 7,
        TYPE_ID = 8,
    };
    public enum AttributeType
    {
        TYPE_NONE = 0,
        TYPE_INT = 1,
        TYPE_FLOAT = 2,
        TYPE_STRING = 3,
        TYPE_WSTRING = 4,
        TYPE_HASH = 5,
        TYPE_INTEGER_ARRAY = 6,
        TYPE_FLOAT_ARRAY = 7,
        TYPE_FILE = 8,
        TYPE_ID_REF = 9,
        TYPE_ID = 10,
        TYPE_ID_HASH_REF = 11,
        TYPE_ID_HASH = 12
    };
    enum Version
    {
        PS3 = 0x10010000,
        PSVITA = 0x110,
        PSM = 0x100,
        UNKNOWN
    };
    public class TypingInformation
    {
        public string key;
        public string value;
    }

    public class LoopbackHandler
    {
        public String FileName;
        public String OldFileName;
        public String ReplacePattern;
        public byte[] FileData;
        public Int64 FilePointer;
    }

}
