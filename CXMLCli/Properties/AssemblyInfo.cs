﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CXML Decompiler")]
[assembly: AssemblyDescription("CXML Decompiler & Compiler")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Li")]
[assembly: AssemblyProduct("CXML Decompiler")]
[assembly: AssemblyCopyright("Public Domain © 2022")]
[assembly: AssemblyTrademark("CXML Decompiler is not a trademark of Li Inc")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("7e3c8d27-de0C-45c1-96ed-a408f3c03b93")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.0.0")]
[assembly: AssemblyFileVersion("1.1.0.0")]
