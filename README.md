# CXML-Decompiler     

v1 RELEASE! Inital Release, can be used on PSMA, RCO and RCS           

v2 RELEASE! - Fixed some issues with PS4: "Sce.HighLevel.UI2.RCO" *nice to know PSM lives on sony :D*      
Added an experimental* VAG decoder, .VAG sound effects embedded in CXML should now automatically be decoded,       
you can also use this feature on its own simply by using the "-iv" argument.             

v3 RELEASE! - Fixed an issue where Float and Interger arrays would not be read correctly.
Renamed Style Table to correct name, (Hash Table)- Fixed endainess of hash table entries

v4 RELEASE! - Float values are now suffixed with a lowercase 'f', Files extracted are now saved as the hash value of there contents (note that ID is the hash of the FILENAME, and not the contents)
fixed an issue that sometimes happened zlib decompression (idek why this was a thing, nor how i fixed it- but it was ...)

v5 RELEASE! - Arrays are now encased in \[], fixed filepaths .

v6 RELEASE! - Fixed a shitload of bugs, and now includes a COMPILER

v7 RELEASE! - Fixed an issue with german language computers where instead of decompiling floats to "7.5" it would be "7,5"
thus breaking arrays sometimes.

v8 RELEASE! - Fixed more locale related issues when compiling, More InvarientCulture set

v9 RELEASE! - Fixed a bug with gim conversion when spaces in path

v10 RELEASE! - Rewrite a bunch of stuff, Add support for PS3 and PSP CXML Files (.p3t, .rco, etc)

v11 RELEASE - Usage changed to require -i and -o instead for more consistancy, also now can read .rcd CXML symbol files

A CXML De/compiler For VITA & PS4 CXML formats. ~ Can decompile PSMA (Playstation Mobile app.info), RCOF (.rco) and RCSF (.rcs)     

Was originally made for reading PSM "app.info" but now it can be used on RCO too :D    
it will output the XML files and embedded resources (and attempt to work out what type of file it is)    

About .GIM and .VAG formats:          
GIM is a Properitary Image Format orignally made for the PS3 and PSP, turns out the vita uses it as well       
there are 3 types used by the vita: 8 bit indexed color, 4 bit indexed color, and argb8888      
the first time you run this program GimConv tool will be downloaded directly from there CDN    
where they conviently leaked them publicly for everyone- Thanks :D

http://e1.dl.playstation.net/e1/downloads/ps3/themes/370/PS3_Custom_Theme_v200.zip       
its just sitting there, in this file. because of course it is.      
     
VAG is a ADPCM Audio Encoding, originally introduced for the original PlayStation 1,         
there is an updated version of it on PSVita and PS4 called "HeVag" there is a decoder built into            
this program, but for encoding u need a SCE CONFIDENTIAL tool >_<             
note: the hevag decoding does NOT support stereo files!    

Download: https://bitbucket.org/SilicaAndPina/cxml-decompiler/downloads/CXMLDecompilerv11.zip    
          
Example Decompile Usage: ```CXMLDecompiler -i common_resource.rco -o common_resource.xml -d -p```           
Example Compile Usage: ```CXMLDecompiler -i common_resource.xml -o common_resource.rco -c```          
        
This is done automatically to any files inside the CXML (providing -p is provided)       
but incase you need to do it manually, you can decode a HEVAG file using the following:           
HEVAG Decode Usage: ```CXMLDecompiler -i sound.vag -o sound.wav --is-vag```         
